<?php
declare(strict_types=1);
namespace ISystem\Controller;

use ISystem\RestClient\ClientInterface;
use ISystem\RestClient\Response\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProducerController extends Controller
{
    const PRODUCER_ENDPOINT_NAME = '/producers';

    /**
     * @Route("/getAllProducers")
     */
    public function getAllAction(): Response
    {
        $client = $this->getRestClient();

        $options = [
            'headers' => ['Accept' => 'application/json']
        ];

        /** @var ResponseInterface $response */
        $response = $client->get(self::PRODUCER_ENDPOINT_NAME, $options);

        return new Response($response->getContent());
    }

    /**
     * @Route("/createOneProducer")
     */
    public function createOneAction(): Response
    {
        $client = $this->getRestClient();

        $body = $this->get('isystem.rest_client.producer.factory')->create(
            1,
            'Example name',
            'http://dummy.url',
            'logo.png',
            999,
            null
            );

        $options = [
            'body' => json_encode($body),
            'headers' => ['Accept' => 'application/json']
        ];

        /** @var ResponseInterface $response */
        $response = $client->post(self::PRODUCER_ENDPOINT_NAME, $options);

        return new Response($response->getContent(), $response->getStatusCode());
    }

    /**
     * @return ClientInterface
     */
    private function getRestClient(): ClientInterface
    {
        return $this->get('isystem.rest_client.rest_api_client');
    }
}
