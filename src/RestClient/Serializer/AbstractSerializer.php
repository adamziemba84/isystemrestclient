<?php
declare(strict_types=1);
namespace ISystem\RestClient\Serializer;

use JsonSerializable;

abstract class AbstractSerializer implements JsonSerializable
{
    /**
     * @return array
     */
    abstract protected function getObjectData(): array;

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return $this->getObjectData();
    }
}
