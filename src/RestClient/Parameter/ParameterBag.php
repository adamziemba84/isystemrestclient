<?php
declare(strict_types=1);
namespace ISystem\RestClient\Parameter;

use ISystem\RestClient\UrlValidatorTrait;

class ParameterBag implements ParameterInterface
{
    use UrlValidatorTrait;

    /** @var string */
    private $baseUri;

    /** @var array */
    private $credentials;

    /** @var string */
    private $apiVersion;

    /**
     * @param string $baseUri
     * @param string $apiVersion
     * @param array $credentials
     */
    public function __construct(string $baseUri, string $apiVersion, array $credentials)
    {
        $this->guardUrl($baseUri);

        $this->baseUri = $baseUri;
        $this->apiVersion = $apiVersion;
        $this->credentials = $credentials;
    }

    /**
     * @return string
     */
    public function getBaseUri(): string
    {
        return $this->baseUri;
    }

    /**
     * @return array
     */
    public function getCredentials(): array
    {
        return $this->credentials;
    }

    /**
     * @return string
     */
    public function getApiVersion(): string
    {
        return $this->apiVersion;
    }
}
