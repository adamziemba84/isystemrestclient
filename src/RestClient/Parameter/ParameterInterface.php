<?php
declare(strict_types=1);
namespace ISystem\RestClient\Parameter;

interface ParameterInterface
{
    /**
     * @return string
     */
    public function getBaseUri(): string;

    /**
     * @return array
     */
    public function getCredentials(): array;

    /**
     * @return string
     */
    public function getApiVersion(): string;
}
