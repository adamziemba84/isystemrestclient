<?php
declare(strict_types=1);
namespace ISystem\RestClient\Producer;

use ISystem\RestClient\Serializer\AbstractSerializer;

class Producer extends AbstractSerializer
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $siteUrl;

    /** @var string */
    private $logoFilename;

    /** @var int */
    private $ordering;

    /** @var string|null */
    private $sourceId;

    /**
     * @param int $id
     * @param string $name
     * @param string $siteUrl
     * @param string $logoFilename
     * @param int $ordering
     * @param string $sourceId
     */
    public function __construct(
        int $id,
        string $name,
        string $siteUrl,
        string $logoFilename,
        int $ordering,
        ?string $sourceId
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->siteUrl = $siteUrl;
        $this->logoFilename = $logoFilename;
        $this->ordering = $ordering;
        $this->sourceId = $sourceId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSiteUrl(): string
    {
        return $this->siteUrl;
    }

    /**
     * @return string
     */
    public function getLogoFilename(): string
    {
        return $this->logoFilename;
    }

    /**
     * @return int
     */
    public function getOrdering(): int
    {
        return $this->ordering;
    }

    /**
     * @return string
     */
    public function getSourceId(): ?string
    {
        return $this->sourceId;
    }

    /**
     * @return array
     */
    protected function getObjectData(): array
    {
        return [
            'producer' => [
                'id' => $this->getId(),
                'name' => $this->getName(),
                'site_url' => $this->getSiteUrl(),
                'logo_filename' => $this->getLogoFilename(),
                'ordering' => $this->getOrdering(),
                'source_id' => $this->getSourceId(),
            ]
        ];
    }
}
