<?php
declare(strict_types=1);
namespace ISystem\RestClient\Producer;

class ProducerFactory
{
    /**
     * @param int $id
     * @param string $name
     * @param string $siteUrl
     * @param string $logoFilename
     * @param int $ordering
     * @param string|null $sourceId
     *
     * @return Producer
     */
    public function create(
        int $id,
        string $name,
        string $siteUrl,
        string $logoFilename,
        int $ordering,
        ?string $sourceId
    ): Producer {
        return new Producer($id, $name, $siteUrl, $logoFilename, $ordering, $sourceId);
    }
}
