<?php
declare(strict_types=1);
namespace ISystem\RestClient;

use ISystem\RestClient\Exception\RequestFailedException;
use ISystem\RestClient\HttpClient\HttpClientInterface;
use ISystem\RestClient\Parameter\ParameterInterface;
use ISystem\RestClient\Request\RequestFactoryInterface;
use ISystem\RestClient\Response\ResponseInterface;

class RestApiClient implements ClientInterface
{
    /** @var HttpClientInterface */
    private $httpClient;

    /** @var ParameterInterface */
    private $parameter;

    /** @var RequestFactoryInterface */
    private $requestFactory;

    /**
     * @param HttpClientInterface $httpClient
     * @param RequestFactoryInterface $requestFactory
     * @param ParameterInterface $parameter
     */
    public function __construct(
        HttpClientInterface $httpClient,
        RequestFactoryInterface $requestFactory,
        ParameterInterface $parameter
    ) {
        $this->httpClient = $httpClient;
        $this->requestFactory = $requestFactory;
        $this->parameter = $parameter;
    }

    /**
     * @param string $endpoint
     * @param array $options
     * @throws RequestFailedException
     *
     * @return ResponseInterface
     */
    public function post(string $endpoint, array $options = []): ResponseInterface
    {
        return $this->send('POST', $endpoint, $options);
    }

    /**
     * @param string $endpoint
     * @param array $options
     * @throws RequestFailedException
     *
     * @return ResponseInterface
     */
    public function get(string $endpoint, array $options = []): ResponseInterface
    {
        return $this->send('GET', $endpoint, $options);
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param array $options
     * @throws RequestFailedException
     *
     * @return ResponseInterface
     */
    private function send(string $method, string $endpoint, array $options = []): ResponseInterface
    {
        $request = $this->requestFactory->create(
            $method,
            $this->getUri($endpoint),
            $this->appendCredentials($options)
        );

        return $this->httpClient->makeRequest($request);
    }

    /**
     * @param string $endpoint
     *
     * @return string
     */
    private function getUri(string $endpoint): string
    {
        return sprintf('%s/%s%s', $this->parameter->getBaseUri(), $this->parameter->getApiVersion(), $endpoint);
    }

    /**
     * @param array $options
     *
     * @return array
     */
    private function appendCredentials(array $options): array
    {
        $options['auth'] = $this->parameter->getCredentials();

        return $options;
    }
}
