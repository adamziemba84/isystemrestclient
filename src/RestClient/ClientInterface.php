<?php
declare(strict_types=1);
namespace ISystem\RestClient;

use ISystem\RestClient\Response\ResponseInterface;

interface ClientInterface
{
    /**
     * @param string $endpoint
     * @param array $options
     *
     * @return ResponseInterface
     */
    public function post(string $endpoint, array $options = []): ResponseInterface;

    /**
     * @param string $endpoint
     * @param array $options
     *
     * @return ResponseInterface
     */
    public function get(string $endpoint, array $options = []): ResponseInterface;
}
