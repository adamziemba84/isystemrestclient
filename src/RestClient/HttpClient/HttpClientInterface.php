<?php
declare(strict_types=1);
namespace ISystem\RestClient\HttpClient;

use ISystem\RestClient\Exception\RequestFailedException;
use ISystem\RestClient\Request\RequestInterface;
use ISystem\RestClient\Response\ResponseInterface;

interface HttpClientInterface
{
    /**
     * @param RequestInterface $request
     * @throws RequestFailedException
     *
     * @return ResponseInterface
     */
    public function makeRequest(RequestInterface $request): ResponseInterface;
}
