<?php
declare(strict_types=1);
namespace ISystem\RestClient\HttpClient;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use ISystem\RestClient\Exception\RequestFailedException;
use ISystem\RestClient\Request\RequestInterface;
use ISystem\RestClient\Response\Response;
use ISystem\RestClient\Response\ResponseInterface;

class HttpClientAdapter implements HttpClientInterface
{
    /** @var ClientInterface */
    private $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param RequestInterface $request
     * @throws RequestFailedException
     *
     * @return ResponseInterface
     */
    public function makeRequest(RequestInterface $request): ResponseInterface
    {
        try {
            $response = $this->client->request($request->getMethod(), $request->getUri(), $request->getOptions());
            return new Response($response);
        } catch (GuzzleException $exception) {
            throw new RequestFailedException($exception->getMessage());
        }
    }
}
