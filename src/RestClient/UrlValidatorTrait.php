<?php
declare(strict_types=1);
namespace ISystem\RestClient;

use InvalidArgumentException;

trait UrlValidatorTrait
{
    /**
     * @param string $url
     * @throws InvalidArgumentException
     */
    private function guardUrl(string $url): void
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new InvalidArgumentException(sprintf('"%s" is not valid url.', $url));
        }
    }
}
