<?php
declare(strict_types=1);
namespace ISystem\RestClient\Response;

use GuzzleHttp\Psr7\Stream;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

class Response implements ResponseInterface
{
    /** @var ResponseInterface */
    private $response;

    /**
     * @param PsrResponseInterface $response
     */
    public function __construct(PsrResponseInterface $response)
    {
        $this->response = $response;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->response->getStatusCode();
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        /** @var Stream $stream */
        $stream = $this->response->getBody();

        return $stream->getContents();
    }
}
