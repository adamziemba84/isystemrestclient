<?php
declare(strict_types=1);
namespace ISystem\RestClient\Response;

interface ResponseInterface
{
    /**
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * @return string
     */
    public function getContent(): string;
}
