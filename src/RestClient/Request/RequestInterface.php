<?php
declare(strict_types=1);
namespace ISystem\RestClient\Request;

interface RequestInterface
{
    /**
     * @return string
     */
    public function getMethod(): string;

    /**
     * @return string
     */
    public function getUri(): string;

    /**
     * @return array
     */
    public function getOptions(): array;
}
