<?php
declare(strict_types=1);
namespace ISystem\RestClient\Request;

class RequestFactory implements RequestFactoryInterface
{
    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     *
     * @return Request
     */
    public function create(string $method, string $uri, array $options): Request
    {
        return new Request($method, $uri, $options);
    }
}
