<?php
declare(strict_types=1);
namespace ISystem\RestClient\Request;

interface RequestFactoryInterface
{
    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     *
     * @return Request
     */
    public function create(string $method, string $uri, array $options): Request;
}
