<?php
declare(strict_types=1);
namespace ISystem\Tests\RestClient\Response;

use InvalidArgumentException;
use ISystem\RestClient\Request\Request;
use ISystem\RestClient\Request\RequestInterface;
use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{
    /**
     * @test
     */
    public function itKeepsDataForRequest()
    {
        $method = 'POST';
        $uri = 'http://dummy.url';
        $options = [];

        $response = $this->getRequest($method, $uri, $options);

        $this->assertEquals($method, $response->getMethod());
        $this->assertEquals($uri, $response->getUri());
        $this->assertEquals($options, $response->getOptions());
    }

    /**
     * @test
     */
    public function itThrowsAnExceptionWhenInvalidUriProvided()
    {
        $method = 'POST';
        $uri = 'invalid url';
        $options = [];

        $this->expectException(InvalidArgumentException::class);

        $response = $this->getRequest($method, $uri, $options);

        $this->assertEquals($method, $response->getMethod());
        $this->assertEquals($uri, $response->getUri());
        $this->assertEquals($options, $response->getOptions());
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     *
     * @return RequestInterface
     */
    private function getRequest(string $method, string $uri, array $options): RequestInterface
    {
        return new Request($method, $uri, $options);
    }
}
