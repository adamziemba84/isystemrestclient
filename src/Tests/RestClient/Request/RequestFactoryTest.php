<?php
declare(strict_types=1);
namespace ISystem\Tests\RestClient\Request;

use ISystem\RestClient\Request\Request;
use ISystem\RestClient\Request\RequestFactory;
use ISystem\RestClient\Request\RequestInterface;
use PHPUNit\Framework\TestCase;

class RequestFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function itCreatesRequestObject()
    {
        $result1 = $this->getRequest();
        $result2 = $this->getRequest();

        $this->assertInstanceOf(Request::class, $result1);
        $this->assertNotSame($result1, $result2);
    }

    /**
     * @return RequestInterface
     */
    private function getRequest(): RequestInterface
    {
        return (new RequestFactory())->create('test_method', 'http://dummy.url', []);
    }
}
