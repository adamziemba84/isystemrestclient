<?php
declare(strict_types=1);
namespace ISystem\Tests\RestClient\Response;

use GuzzleHttp\Psr7\Response as PsrResponse;
use ISystem\RestClient\Response\Response;
use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class ResponseTest extends TestCase
{
    /** @var ResponseInterface|MockInterface */
    private $psrResponseMock;

    /** @var Response */
    private $response;

    protected function setUp()
    {
        $this->psrResponseMock = Mockery::mock(PsrResponse::class);
        $this->response = new Response($this->psrResponseMock);
    }

    /**
     * @test
     */
    public function itWrapsPsrResponseObject()
    {
        $statusCode = 200;
        $testContent = 'test content';

        $this->psrResponseMock->shouldReceive('getStatusCode')->andReturn($statusCode);
        $this->psrResponseMock->shouldReceive('getBody->getContents')->andReturn($testContent);

        $this->assertEquals($statusCode, $this->response->getStatusCode());
        $this->assertEquals($testContent, $this->response->getContent());
    }
}
