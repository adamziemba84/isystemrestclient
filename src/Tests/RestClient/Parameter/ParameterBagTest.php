<?php
declare(strict_types=1);
namespace ISystem\Tests\RestClient\Request;

use ISystem\RestClient\Parameter\ParameterBag;
use PHPUNit\Framework\TestCase;

class ParameterBagTest extends TestCase
{
    /**
     * @test
     */
    public function itIsPassingValidParametersToRequest()
    {
        $baseUri = 'http://dummy.url';
        $apiVersion = 'api_version';
        $credentials = [];

        $parameterBag = $this->getParameterBag($baseUri, $apiVersion, $credentials);

        $this->assertEquals($baseUri, $parameterBag->getBaseUri());
        $this->assertEquals($apiVersion, $parameterBag->getApiVersion());
        $this->assertEquals($credentials, $parameterBag->getCredentials());
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenProvideInvalidDataToParamaterBag()
    {
        $baseUri = 'invalid_base_uri';
        $apiVersion = 'api_version';
        $credentials = [];

        $this->expectException(\InvalidArgumentException::class);

        $this->getParameterBag($baseUri, $apiVersion, $credentials);
    }

    /**
     * @param string $baseUri
     * @param string $apiVersion
     * @param array $credentials
     *
     * @return ParameterBag
     */
    private function getParameterBag(string $baseUri, string $apiVersion, array $credentials): ParameterBag
    {
        return new ParameterBag($baseUri, $apiVersion, $credentials);
    }
}
